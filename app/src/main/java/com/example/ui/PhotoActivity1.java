package com.example.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.InputStream;

public class PhotoActivity1 extends AppCompatActivity {
    private Button selectBtn;
    private Button cancelBtn;
    ImageView facePhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo1);

        facePhoto = findViewById(R.id.facePhoto);
        cancelBtn = findViewById(R.id.cancelBtn);
        selectBtn = findViewById(R.id.selectBtn);

        selectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, 1);

                // 갤러리에서 여러 이미지 선택
                //intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                facePhoto.setImageResource(0);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                try {
                    // 선택한 이미지에서 비트맵 생성
                    InputStream in = getContentResolver().openInputStream(data.getData());
                    Bitmap img = BitmapFactory.decodeStream(in);
                    in.close();

                    // 이미지 크기 줄이기
                    img = img.createScaledBitmap(img,img.getWidth()/2, img.getHeight()/2,true);
                    // 이미지 표시
                    facePhoto.setImageBitmap(img);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void back(View v) {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    public void start(View v) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}