package com.example.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    TextView id, pw;
    /*
    static {
        if(OpenCVLoader.initDebug()) {
            Log.d("MainActivity:", "Opencv is loaded");
        }
        else {
            Log.d("MainActivity:", "Opencv failed to load");
        }
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
    }

    public void login(View v) {
        Intent intent = new Intent(this, MainActivity.class);

        id = findViewById(R.id.idText);
        pw = findViewById(R.id.pwText);

        // DB 추가 작업 필요
        if(id.getText().toString().equals("ds1") && pw.getText().toString().equals("1234")) {
            startActivity(intent);
        }
        else {
            Toast.makeText(this, "아이디 및 비밀번호를 다시 확인해주세요", Toast.LENGTH_LONG).show();
            id.setText("");
            pw.setText("");
        }
    }

    public void register(View v) {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }
}